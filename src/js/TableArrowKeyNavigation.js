/* This allows the user to navigate tables with editable cells */

class TableArrowKeyNavigation {
  constructor () {
    this._currentCell = HTMLElement
    this._currentRow = HTMLElement
    this._currentCellIndex = 0
    this._currentRowIndex = 0
  }

  navigate (event) {
    // Functional

    this._currentCell = this.getCurrentCell()
    this._currentRow = this.getCurrentRow(this._currentCell)
    this._currentCellIndex = this.getCellIndex(this._currentCell)
    this._currentRowIndex = this.getRowIndex(this._currentRow)

    if (event.keyIdentifier === "Down") this._arrowDown()
    if (event.keyIdentifier === "Left") this._arrowLeft()
    if (event.keyIdentifier === "Right") this._arrowRight()
    if (event.keyIdentifier === "Up") this._arrowUp()
  }

  focusCell (cell) {
    // Pure
    let result = cell.firstChild;

    while (result.tagName !== "INPUT") {
      result = result.firstChild
    }

    result.focus()
  }

  getCurrentCell () {
    // Pure
    let result = document.activeElement;

    while (result.tagName !== "TD") {
      result = result.parentElement
    }

    return result
  }

  getCurrentRow (activeCell) {
    // Pure
    return activeCell.parentElement
  }

  getCellIndex (cell) {
    // Pure
    const row = cell.parentNode.children
    return Array.prototype.indexOf.call(row, cell)
  }

  getRowIndex (row) {
    // Pure
    const tbody = row.parentNode.children
    return Array.prototype.indexOf.call(tbody, row)
  }


  _arrowDown () {
    // Pure
    const totalRows = this._currentRow.parentElement.children.length

    if (this._currentRowIndex < totalRows - 1) {
      const nextRow = this._currentRow.nextSibling.childNodes
      const nextCell = nextRow[this._currentCellIndex]
      this.focusCell(nextCell)
    }
  }

  _arrowLeft () {
    // Pure
    const previusCell = this._currentCell.previousSibling
    this.focusCell(previusCell)
  }

  _arrowRight () {
    // Pure
    const nextCell = this._currentCell.nextSibling
    this.focusCell(nextCell);
  }

  _arrowUp () {
    // Pure
    if (this._currentRowIndex > 0) {
      const previusRow = this._currentRow.previousSibling.childNodes
      const previusCell = previusRow[this._currentCellIndex]
      this.focusCell(previusCell);
    }
  }
}

export default TableArrowKeyNavigation
