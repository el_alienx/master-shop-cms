/* This class handles the resizing of images using HTML canvas element and the Pica library */

class PicaImageResizer {
  resize (file, maxWidth, quality) {
    return new Promise(resolve => {
      (async () => {
        const image = await this._createImage(file);
        const canvases = await this._createCanvases(image, maxWidth);
        const resize = await this._picaResize(canvases, quality);
        await resolve(resize)
      })();
    })
  }

  _createImage (file) {
    // Pure
    return new Promise(resolve => {
      const image = new Image();

      image.src = window.URL.createObjectURL(file);
      image.onload = () => resolve(image);
    });
  }

  _createCanvases (image, maxWidth) {
    // Pure
    const originalCanvas = document.createElement("canvas");
    const resizedCanvas = document.createElement("canvas");
    const responsiveWidth = image.width > maxWidth ? maxWidth : image.width;

    originalCanvas.width = image.width;
    originalCanvas.height = image.height;
    originalCanvas.getContext("2d").drawImage(image, 0, 0);

    resizedCanvas.width = image.width > maxWidth ? maxWidth : image.width;
    resizedCanvas.height = image.height * responsiveWidth / image.width;

    return [originalCanvas, resizedCanvas]
  }

  _picaResize (canvases, quality) {
    // Pure
    return new Promise(resolve => {
      const pica = require("pica")();

      pica
        .resize(canvases[0], canvases[1])
        .then(response => pica.toBlob(response, "image/jpeg", quality))
        .then(resolve);
    });
  }
}

export default PicaImageResizer