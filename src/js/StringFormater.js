/* This class handles all the text format and conversion needs of the project */

import moment from 'moment'

class StringFormater {

  static formatDate(timeStamp, format) {
    var date = timeStamp.toDate()
    return moment(date).format(format)
  }

  static formatPrice(number) {
    return `$${number.toFixed(2)}`
  }
}

export default StringFormater