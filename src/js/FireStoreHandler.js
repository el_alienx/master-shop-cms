/* This class handle the Firestore network events */

class FireStoreHandler {

  constructor () {
  }

  // CRUD methods
  // Create
  static createDocument (reference, data) {
    // Functional 

    return new Promise((resolve, reject) => {
      reference.add(data)
        .then(docRef => resolve(docRef))
        .catch(error => reject(error));
    })
  }

  static createDocumentWithId (reference, id, data) {
    // Functional 

    return new Promise((resolve, reject) => {
      reference.doc(id).set(data)
        .then(docRef => resolve(docRef))
        .catch(error => reject(error));
    })
  }

  // Read
  static readDocument (reference, id) {
    // Functional 

    return new Promise((resolve, reject) => {
      reference.doc(id).get()
        .then(function (doc) {
          if (doc.exists) {
            console.log("Document data:", doc.data());
            resolve(doc.data())
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }
        })
        .catch(error => reject(error));
    })
  }

  static readCollection (reference) {
    // Functional 

    return new Promise((resolve, reject) => {
      reference.get()
        .then(querySnapshot => {
          let array = this.parseCollection(querySnapshot)
          resolve(array)
        })
        .catch(error => reject(error));
    })
  }

  // Update
  static updateDocument (reference, id, data) {
    // Functional 

    return new Promise((resolve, reject) => {
      return reference.doc(id).update(data)
        .then(success => resolve(success))
        .catch(error => reject(error));
    })
  }

  // Delete
  static deleteDocument (reference, id) {
    // Functional 

    return new Promise((resolve, reject) => {
      reference.doc(id).delete()
        .then(success => resolve(success))
        .catch(error => reject(error));
    })
  }

  // Helper methods
  static parseCollection (querySnapshot) {
    // Pure

    let collection = []
    let document = {}

    querySnapshot.forEach(doc => {
      document = { id: doc.id, ...doc.data() };
      collection.push(document);
    });

    return collection
  }
}

export default FireStoreHandler