import Vue from 'vue'
import Router from 'vue-router'

// Routes
import Schedule from './pages/Schedule'
import FoodMenu from './pages/FoodMenu'
import FoodCategory from './pages/FoodCategory'
import Login from './pages/Login'
import Orders from './pages/Orders'
import Dashboard from './pages/Dashboard'
import Reset from './pages/Reset'

Vue.use(Router)

export default new Router({
  routes: [
    {
      component: Dashboard,
      name: 'Dashboard',
      path: '/tablero-de-control'
    },
    {
      component: FoodMenu,
      name: 'FoodMenu',
      path: '/menu-de-comida'
    },
    {
      component: FoodCategory,
      name: 'FoodCategory',
      path: 'menu-de-comida/:id'
    },
    {
      component: Login,
      name: 'Login',
      path: '/'
    },
    {
      component: Orders,
      name: 'Orders',
      path: '/ordenes'
    },
    {
      component: Schedule,
      name: 'Schedule',
      path: '/horarios-de-entrega'
    },
    {
      component: Reset,
      name: 'Reset',
      path: '/reiniciar-clave'
    }
  ]
})
