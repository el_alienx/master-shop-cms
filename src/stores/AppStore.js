import firebase from "../js/firebase"
import FireStoreHandler from "../js/FireStoreHandler";


const AppStore = {
  data: {
    owner: {},
    store: {},
    uid: ""
  },
  getStorage () {
    // Impure
    // Depends of state

    const store = this.data.store
    return firebase.storage().ref('stores/' + store)
  },
  getUsersReference () {
    // Pure
    return firebase.firestore().collection('users')
  },
  getStoreReference () {
    // Impure
    // Depends of state

    const store = this.data.owner.store
    return firebase.firestore().collection('stores').doc(store)
  },
  getStoreData () {
    // Impure
    // Mutate state

    // Refactor
    const reference = firebase.firestore().collection('stores')
    const store = this.data.owner.store

    FireStoreHandler.readDocument(reference, store)
      .then(document => {
        this.data.store = document
      })
  },
  parseCollection (querySnapshot) {
    // Pure

    let collection = []
    let document = {}

    querySnapshot.forEach(doc => {
      document = { id: doc.id, ...doc.data() };
      collection.push(document);
    });

    return collection
  },
  // Auth methods
  signIn (email, password, session) {
    // Impure
    // Mutate state (uid, owner, store)
    return new Promise((resolve, reject) => {
      Promise.resolve()
        .then(() => {
          return this._validateCredentials(email, password)
        })
        .then(uid => {
          this.data.uid = uid
          return this._getLoggedUser(uid)
        })
        .then(loggedUser => {
          this.data.owner = loggedUser.data()

          this._setSession(email, password, session)
          resolve(true)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  logout () {
    return new Promise(resolve => {
      firebase.auth().signOut()
        .then(() => {
          this.data.owner = null
          this.data.store = ""
          this.data.uid = ""
          this._clearSession()
          resolve(true)
        }).catch(error => {
          console.error('Logout error', error.code, error.message)
        });
    })
  },
  _validateCredentials (email, password) {
    // Functional 

    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(success => {
          resolve(success.user.uid)
        })
        .catch(error => reject(error));
    })
  },
  _getLoggedUser (uid) {
    // Functional 

    return new Promise((resolve, reject) => {
      firebase
        .firestore()
        .collection("owners")
        .doc(uid)
        .get()
        .then(success => resolve(success))
        .catch(error => reject(error));
    })
  },
  _setSession (email, password, session) {
    // Impure
    // Mutate state of localstorage and sessionstorage

    sessionStorage.setItem('email', email)
    sessionStorage.setItem('password', password)

    if (session) {
      localStorage.setItem('email', email)
      localStorage.setItem('password', password)
    }
  },
  _clearSession () {
    localStorage.removeItem('email')
    localStorage.removeItem('password')
    sessionStorage.removeItem('email')
    sessionStorage.removeItem('password')
  }
}

export default AppStore