const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

exports.createOrder = functions.firestore
  .document('stores/{storeId}/orders/{orderId}')
  .onCreate((snap, context) => {
    const newValue = snap.data();
    const name = newValue.name
    const location = newValue.address.urbanization

    // perform desired operations ...
    console.log(`A new order was created to be delivered by ${name} to ${location}`)
  });

exports.updateOrder = functions.firestore
  .document('users/{userId}/orders/{storeId}/orders/{orderId}')
  .onUpdate((change, context) => {
    const newValue = change.after.data();
    const store = newValue.store_name
    const registrationToken = newValue.notificationToken
    const messageStatus = newValue.statusMessage

    // perform desired operations ...
    // See documentation on defining a message payload.
    var payload = {
      notification: {
        title: `Pedido de ${store}`,
        body: messageStatus
      },
      token: registrationToken,
      // Apple specific settings
      apns: {
        headers: {
          'apns-priority': '10',
        },
        payload: {
          aps: {
            sound: 'default',
          }
        },
      }
    };

    // Send a message to the device corresponding to the provided
    // registration token.
    admin.messaging().send(payload)
      .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent this message:', response);
      })
      .catch((error) => {
        console.log('Error sending this message:', error);
      });
  });